# DEVPHP

Projeto WEB desenvolvido em PHP <br>


## Tecnologias

O ambiente de desenvolvimento foi construído com as tecnologias abaixo.

Docker <br>
Linguagem de Programação PHP <br>
Mysql <br>
Bootstrap <br>
IDE Visual Studio Code <br>


```
cd existing_repo
git remote add origin https://gitlab.com/engenheiro.paulo.cezar/devphp.git
git branch -M main
git push -uf origin main
```


### Para subir os containers execute o docker-compose

```
# docker-compose up -d
```

### Descrição do Projeto

Na pasta do projeto existem duas pastas mysql e php. Na pasta mysql ficarão os dados e na pasta do php os arquivos fonte. <br>

Este é um pequeno projeto para testar os conhecimentos da disciplina WEB I do curso de Análise e Desenvovlimento. Trata-se de um pequeno cadastro de produtos com CRUD completo.

Lista de Produtos <br>
Cadastro de Produtos <br>
Exclusão de Produtos <br>

Conhecimentos envolvidos: <hr>
HTML <br>
CSS <br>
Linguagem de Programação Javascript <br>
Linguagem de Programação PHP <br>
Banco de Dados MYSQL <br>


<?php 
    require_once('conecta.php');
    require_once('banco-produto.php');

    $id = $_GET['id'];
    removeProduto($conexao, $id);
    header("Location: produto-lista.php?removido=true");
    
    // Com esse comando peço para o PHP parar de executar.
    die();
?>    

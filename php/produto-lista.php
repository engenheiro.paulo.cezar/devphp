<?php 
    require_once('header.php'); 
    require_once('conecta.php');
    require_once('banco-produto.php');
    $produtos = listaProdutos($conexao);

?>

<?php 
    if(array_key_exists("removido", $_GET) && $_GET['removido']=='true') { ?>

<p class="alert-success">Produto apagado com sucesso.</p>

<?php } ?>



<table class="table table-striped table-bordered">
<tr>
    <td>Produto</td>
    <td>Valor</td>
    <td>Ação</td>
</tr>

<?php 
    foreach($produtos as $produto) { 
?>

    <tr>
        <td><?=$produto['nome'] ?> </td>
        <td><?=$produto['preco'] ?> </td>
        <td><a href="remove-produto.php?id=<?=$produto['id'] ?>" class="text-danger">remover </a></td>
    </tr>
<?php
    }
?>

</table>

<?php require_once('footer.php'); ?>
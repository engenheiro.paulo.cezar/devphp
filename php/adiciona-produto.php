<?php 
    require_once('header.php'); 
    require_once('conecta.php');
    require_once('banco-produto.php');
?>


    <?php
        $nome = $_GET["nome"];
        $preco = $_GET["preco"];                     

        // Executando a query no banco de dados
        if (insereProduto($conexao, $nome, $preco)) { ?>

        <p class="text-success">
            Produto <?= $nome ?>, <?= $preco ?> adicionado com sucesso.
        </p>
            
        <?php   
        } else {
            
            ?>

        <p class="text-danger">
            Produto <?= $nome ?>, <?= $preco ?> não foi adicionado.
        </p>
        <?php

        } ?>
        
<?php require_once('footer.php'); ?>